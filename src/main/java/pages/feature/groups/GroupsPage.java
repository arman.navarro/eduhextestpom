package pages.feature.groups;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class GroupsPage extends BasePage {

	public GroupsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
 //Page Methods
	@Step ("Verify Groups Side Navigation: {0} step...")
	public GroupsPage Groupsbtn () {
		click(GroupsbtnBy);
		
		return this;
	}
	
	@Step("Verify Groups Main view: {0} step...")
	public GroupsPage VerifyGroupsMainView (String expectedText) {
		assertEquals(GroupsMainViewTable, expectedText);
		
		return this;
	}
}
