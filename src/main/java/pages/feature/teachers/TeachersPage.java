package pages.feature.teachers;


import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;


public class TeachersPage extends BasePage {

	public TeachersPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	

	@Step("Navigate to Users: {0} step...")
	public TeachersPage Users () {
		click(UsersbtnBy);
		return this;
	}
	@Step("View Teachers: {0} step...")
	public TeachersPage Teachersbtn () {
		
		click(TeachersbtnBy);
		
		return this;
	}

	@Step("View Teachers Main view {0} step...")
	public TeachersPage VerifyTeachersMainView (String expectedText) {
		assertEquals(TeachersMainViewTable, expectedText);
		
		return this;
	}

}
