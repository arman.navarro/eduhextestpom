package pages.feature.roles;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;


public class RolesPage extends BasePage {
	
		public RolesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
		

		@Step("View Roles: {0} step..")
		public RolesPage Rolesbtn ()
		{
			click(RolesbtnBy);
			return this;
		}

		@Step("Verify Roles Main view: {0} step...")
		public RolesPage VerifyRolesMainView (String expectedText) {
			assertEquals(RolesMainViewTable, expectedText);
			
			return this;
		}
}
