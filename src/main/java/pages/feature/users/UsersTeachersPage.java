package pages.feature.users;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class UsersTeachersPage extends BasePage {

	public UsersTeachersPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@Step("Verify Users Side Navigation: {0} step...")
	public UsersTeachersPage UsersAllbtn () {
		click(UsersAllbtnBy);
		
		return this;
	}
	
	@Step("Verify Users- Teachers Side Navigation: (0} step...")
	public UsersTeachersPage UsersTeacherbtn () {
		click(TeachersbtnBy);
		
		return this;
	}
	
	@Step("Verify Users- Teachers Main view")
	public UsersTeachersPage UsersTeachersMainView (String expectedText) {
		assertEquals(TeachersMainViewTable, expectedText);
		
		return this;
	}
}
