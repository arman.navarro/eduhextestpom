package pages.feature.users;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class UsersStudentsPage extends BasePage {

	public UsersStudentsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@Step("Verify Users Side Navigation: {0} step...")
	public UsersStudentsPage UsersAllbtn () {
		click(UsersAllbtnBy);
		return this;
	}
	
	@Step("Verify Users- Students Side Navigate: {0} step...")
	public UsersStudentsPage UsersStudentbtn () {
		click(StudentsbtnBy);
		
		return this;
	}
	
	@Step("Verify Users- Students Main view")
	public UsersStudentsPage UsersStudentsMainView (String expectedText) {
		assertEquals(StudentsMainViewTable, expectedText);
		
		return this;
	}
	

}
