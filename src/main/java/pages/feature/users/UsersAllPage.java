package pages.feature.users;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class UsersAllPage extends BasePage {
	//*********Constructor*********
    public UsersAllPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@Step("Verify Users Side Navigation: {0} step...")
    public UsersAllPage Usersbtn () {
    	click(UsersbtnBy);
    	
    	return this;
    }
    
    @Step("Verify Users All Side Navigation: {0} step...")
    public UsersAllPage UsersAllbtn () {
    	click(UsersAllbtnBy);
    	
    	return this;
    }

    @Step ("Verify Users Main view: {0} step...")
    public UsersAllPage VerifyUsersMainView (String expectedText) {
    	assertEquals(UsersMainViewTable, expectedText);
    	
    	return this;
    }
    
    @Step ("Drag and Drop")
    public UsersAllPage DragNDrop () {
    	getAction2().clickAndHold(usernameBy1).moveToElement(passwordBy1).release(passwordBy1).perform();
    	return this;
    }

    @Step ("Click Add User: {0} step...")
    public UsersAllPage AddUserbtn () {
    	click(NewUserAddbtn);
    	
    	return this;
    }

    @Step ("Fill In User Information First Name: {0}, LastName: {1}, Username: {2}, Password: {3}"
    		+ " Email: {4}, Mobile Numer: {5}, method: {method} step... ")
    public UsersAllPage AddNewUserTeacher (
    		String FirstName, String LastName,
    		String Username, String Password,
    		String Email, String MobileNumber
    		) {
    	
    	//Enter FirstName
    	writeText(UsersFirstName, FirstName);
    	
    	//Enter LastName
    	writeText(UsersLastName, LastName);
    	
    	//Enter Username
    	writeText(UsersUsername, Username);
    	
    	//Enter Password
    	writeText(UsersPassword, Password);
    	
    	//Enter Email
    	writeText(UsersEmail, Email);
    	
    	//Enter Mobile Number
    	writeText(UsersMobileNum, MobileNumber);
    	
    	
    	return this;	
    }

    @Step ("Select Role for user: {0}, method: {method} step...")
    public UsersAllPage SelectRole () {
    	click(UsersRole);
    	
    	return this;
    }
  
    @Step ("Assign User role: {0}, method: {method} step...")
    public UsersAllPage SelectRoleUsers () {
    	click(UsersRoleUsers);
    	return this;
    }
    
    @Step ("Click Outside: {0}, method: {method} step...")
    public UsersAllPage ClickOut1 () {
    	click(UsersClickOut);
    	
    	return this;
    }
    
    @Step ("Click Outside: {0}, method: {method} step...")
    public UsersAllPage ClickOut2 () {
    	click(UsersClickOut);
    	
    	return this;
    }

    @Step ("Select User type: {0}, method: {method} step...")
    public UsersAllPage SelectUsersType () {
    	click(UsersType);
    	
    	return this;
    }

    @Step ("Select User type as Teacher: {0}, method: {method} step...")
    public UsersAllPage SelectUsersTypeTeacher () {
    	click(UsersTypeTeacher);
    	
    	return this;
    }

    @Step("Click Save button: {0}, method: {method} step...")
    public UsersAllPage SaveUserbtn () {
    	click(UsersSavebtnBy);
    	
    	return this;
    }
    //Go to UsersAllPage
}
