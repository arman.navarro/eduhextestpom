package pages.feature.lessons;


import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;



public class LessonsPage extends BasePage {
	//Contructor
	public LessonsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	

	@Step("Verify Lessons Side Navigation: {0} step...")
	public LessonsPage Lessonsbtn () {
		click(LessonsbtnBy);
		
		return this;
	}

	@Step("Verify Lessons Main View: {0} step...")
	public LessonsPage VerifyLessonsMainView (String expectedText) {
		assertEquals(LessonsMainViewTable, expectedText);
		
		return this;
	}
	
}
