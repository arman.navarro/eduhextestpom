package pages.feature.search.searchClassesPage;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class searchClassesPage extends BasePage {

	public searchClassesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
			
;			//Page Methods
		
			@Step ("Verify Classes Side Navigation: {0} step...")
			public searchClassesPage Classesbtn () {
				click(ClassesbtnBy);
				
				return this;	
			}

			@Step("Verify Classes Main view: {0} step...")
			public searchClassesPage VerifyClassesMainView (String expectedText) {
				assertEquals(ClassesMainViewTable, expectedText);
				
				return this;
			}
			
			@Step("Search Classes")
			public searchClassesPage Search_a_Class (String SClass) {
				
				writeText(SearchBoxBy, SClass);
				
				return this;
			}
			
			@Step("Look at Search Results")
			public searchClassesPage Search_Result (String expectedText ) {
				assertEquals(SResult, expectedText);
				
				return this;
			}

}
