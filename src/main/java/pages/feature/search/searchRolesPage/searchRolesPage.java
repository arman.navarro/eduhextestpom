package pages.feature.search.searchRolesPage;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class searchRolesPage extends BasePage {

	public searchRolesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
		//Page Methods
		@Step ("Verify Roles Side Navigation: {0} step...")
		public searchRolesPage Rolesbtn () {
			click(RolesbtnBy);
			
			return this;	
		}

		@Step("Verify Roles Main view: {0} step...")
		public searchRolesPage VerifyRolesMainView (String expectedText) {
			assertEquals(RolesMainViewTable, expectedText);
			
			return this;
		}
		
		@Step("Search Roles")
		public searchRolesPage Search_a_Role (String SRole) {
			
			writeText(SearchRolesBy, SRole);
			
			return this;
		}
		
		@Step("Look at Search Results")
		public searchRolesPage Search_Result (String expectedText ) {
			assertEquals(SResult, expectedText);
			
			return this;
		}


}
