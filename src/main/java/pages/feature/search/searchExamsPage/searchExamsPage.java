package pages.feature.search.searchExamsPage;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class searchExamsPage extends BasePage {

	public searchExamsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
		
		//Page Methods
		
		@Step ("Verify My Exams side Navigation: {0} step...")
		public searchExamsPage myExamsbtn () {
			click(ExamsbtnBy);
			
			
			return this;
		}
		
		@Step("Verify My Exams Template side Navigation: {0} step...")
		public searchExamsPage myExamsTemplatebtn () {
			
			click(ExamsTemplatebtnBy);
			
			return this;
		}
		
		@Step ("Verify My Exams Main view: {0} step...")
		public searchExamsPage Verify_Exam_Template_Main_View (String expectedText) {
			assertEquals(ExamsMainViewTable, expectedText);
			
			return this;
		}

		@Step("Search Exams")
		public searchExamsPage Search_a_Exam (String SExam) {
			writeText(SearchExamTemplatesBy, SExam);
			
			return this;
		}
		
		@Step("Look at Search Results")
		public searchExamsPage Search_Result (String expectedText) {
			assertEquals(SResult, expectedText);
			
			return this;
		}
		
}
