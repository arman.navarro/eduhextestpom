package pages.feature.search.searchGroupsPage;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class searchGroupsPage extends BasePage {

	public searchGroupsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	//Page Methods
	
	@Step("Verify Groups Side Navigation: {0} step...")
	public searchGroupsPage groupsbtn () {
		click(GroupsbtnBy);
		
		return this;
	}
	
	@Step("Verify Groups Main View: {0} step...")
	public searchGroupsPage Verify_GRoups_Main_view (String expectedText) {
		assertEquals(GroupsMainViewTable, expectedText);
		
		return this;
	}
	
	@Step("Search Groups")
	public searchGroupsPage Search_a_Group (String SGroups) {
		writeText(SearchGroupsBy, SGroups);
	
		return this;
	}
	@Step("Look at Search Results")
	public searchGroupsPage Search_Result (String expectedText) {
		assertEquals(SResult, expectedText);
		
		return this;
	}

}
