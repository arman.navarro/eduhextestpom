package pages.feature.search.searchLessonsPage;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class searchLessonsPage extends BasePage {

	public searchLessonsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

		
		//Page Methods
		
		@Step ("Veirfy Lessons Side Nvigation: {0} step...")
		public searchLessonsPage lessonsbtn() {
			click(LessonsbtnBy);
			
			return this;
		}
		
		@Step("Verify Lessons Main View: {0} step...")
		public searchLessonsPage Verify_Lessons_Main_View (String expectedText) {
			assertEquals(LessonsMainViewTable, expectedText);
			
			return this;
		}
		
		@Step("Search Lessons")
		public searchLessonsPage Search_a_Lesson (String SLesson) {
			writeText(SearchLessonsBy, SLesson);
			
			return this;
		}
		
		@Step("Look at Search Results")
		public searchLessonsPage Search_Result (String expectedText) {
			assertEquals(SResult, expectedText);
			
			return this;
		}
		
}
