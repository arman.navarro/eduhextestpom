package pages.feature.students;


import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;


public class StudentsPage extends BasePage {

	public StudentsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	//Page Methods
	@Step("Verify Users Side Navigation: {0} step...")
	public StudentsPage Users () {
		click(UsersbtnBy);
		
		return this;
	}
	@Step ("Verify Students Side Navigation: {0} step...")
	public StudentsPage Studentsbtn () {
		click(StudentsbtnBy);
		
		return this;
	}

	@Step ("Verify Students Main view: {0} step...")
	public StudentsPage VerifyStudentsMainView (String expectedText) {
		assertEquals(StudentsMainViewTable, expectedText);
		
		return this;
	}
}
