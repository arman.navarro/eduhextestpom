package pages.feature.classes;


import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;


public class ClassesPage extends BasePage {

	public ClassesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//Page Methods

	@Step ("Verify Classes Side Navigation: {0} step...")
	public ClassesPage Classesbtn () {
		click(ClassesbtnBy);
		
		return this;	
	}

	@Step("Verify Classes Main view: {0} step...")
	public ClassesPage VerifyClassesMainView (String expectedText) {
		assertEquals(ClassesMainViewTable, expectedText);
		
		return this;
	}
}
