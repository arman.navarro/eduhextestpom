package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class LogoutMe extends BasePage {
	//*********Constructor*********
    public LogoutMe(WebDriver driver) {
		super(driver);
	}
    //*********Web Elements*********
    By logoutBy = By.cssSelector("a.nav-link:nth-child(10)");

	public LogoutMe Logoutbtn () {
		click(logoutBy);
		
		return this;
	}
}

