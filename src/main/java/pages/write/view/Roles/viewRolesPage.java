package pages.write.view.Roles;

import java.util.Random;

import javax.sql.RowSet;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.WebElement;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import pages.BasePage;
import java.util.List;

public class viewRolesPage extends BasePage {

	public viewRolesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	// Get the rows in ur table
	List <WebElement> inCount = driver.findElements(By.cssSelector(".ag-center-cols-container > div:nth-child(2)"));
	int count = inCount.size();
	Random r=new Random();
	int linkNo=r.nextInt(count);
	By View = (By) inCount.get(linkNo);

	

		@Step ("Click randomly at a element: {0} step...")
		public viewRolesPage View_a_Role () {
		driver.findElement(View).click();
			
			return this;
		}
		
		
}
