package pages.write.add.Exams;

import io.qameta.allure.Step;
import pages.BasePage;
import pages.feature.exams.ExamsPage;
import org.openqa.selenium.WebDriver;



public class addExamPage extends BasePage {

	public addExamPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//Page Variables
			//Page Methods
			
	@Step ("Verify My Exams side Navigation: {0} step...")
	public addExamPage myExamsbtn () {
		click(ExamsbtnBy);
		
		
		return this;
	}
	
	@Step("Verify My Exams Template side Navigation: {0} step...")
	public addExamPage myExamsTemplatebtn () {
		
		click(ExamsTemplatebtnBy);
		
		return this;
	}
	
	@Step ("Verify My Exams Main view: {0} step...")
	public addExamPage Verify_Exam_Template_Main_View (String expectedText) {
		assertEquals(ExamsMainViewTable, expectedText);
		
		return this;
	}
	
	@Step ("Ad/Create New Exam: {0} step...")
	public addExamPage AddExambtn () {
		click(byAddExamTemplatebtn);
		
		return this;
	}
	
	@Step ("Fill in Exam Name: {0}, method: {method} step...")
	public addExamPage Fill_in_Exam_Name (String ExamName) {
		writeText(byExamName,ExamName);
		
		return this;
	}/**
	@Step ("Select Exam Type: Quiz")
	public addExamPage ExamType_Dropdown() {
		Select Exam = (ExamTypeDropdown);
		Exam.selectByVisibleText("Quiz");
		
		return this;
	}*/
	
	//Fill in exam type
	@Step("Fill in Exam type: {0} method: {method} step...")
	public addExamPage Fill_in_Exam_type () {
		click(byExamType);
		
		return this;
	}
	
	@Step("Click Out from Time picker")
	public addExamPage Click_Out_Time () {
		
		//Click out
		click(byTimeOut);
		
		return this;
	}
	
	@Step("Select Quiz Exam type: {0}, method {method} step...")
	public addExamPage Select_Exam_Type_Quiz () {
		
		//Select Quiz
		click(byQuizExam);
		
		return this;
	}
	
	@Step("Fill in Exam Start Time: {0} method: {method} step...")
	public addExamPage Click_Exam_Start_Time () {
		click(byExamStartTimeBox);
		
		return this;
	}
	@Step("Fill in Exam Start Time: {0} method: {method} step...")
	public addExamPage Click_Exam_End_Time () {
		click(byExamEndTimeBox);
		
		return this;
	}
	
	@Step("Fill in Exam Start Time: {0} method: {method} step...")
	public addExamPage Select_Exam_Start_Time (String ExamStartTime) {
		writeText(byExamStartTimeBox,ExamStartTime);
		
		return this;
	}
	
	@Step("Fill in Exam End Time: {0} method: {method} step...")
	public addExamPage Select_Exam_End_Time (String ExamEndTime) {
		writeText(byExamEndTimeBox,ExamEndTime);
		
		return this;
	}
	
	@Step("Fill in Exam Start Time: {0}, method: {method} step...")
	public addExamPage Select_Exam_Start_Time_1 () {
		
				//Click start time box
				click(byExamStartTimeBox);
				
				//Click time
				//click(byExamStartTimeFirst);
				
				//Click OK
				click(byExamStartTimeOK);
				
		return this;
	}
	
	@Step("Fill in Exam End Time: {0}, method: {method} step...")
	public addExamPage Select_Exam_End_Time_1 () {
		
				//Click start time box
				click(byExamEndTimeBox);
				
				//Click OK
				click(byExamEndTimeOK);
				
		return this;
	}
	/**
	//Fill in exam information
	@Step ("Fill in Exam Requirements Exam type: {0}, Exam start time: {1}, Exam end time: {2}, method: {method} step...")
	public addExamPage FillinExamRequirements_Dropdown () {
		
		//Select Exam type
		click(byExamType);
		
		//Select Quiz
		click(byQuizExam);
		
		//Click start time box
		click(byExamStartTimeBox);
		
		//Click time
		click(byExamStartTimeFirst);
		
		//Click OK
		click(byExamStartTimeOK);
		
		//Click end time box
		click(byExamEndTimeBox);
		
		//Click time
		click(byExamEndTimeFirst);
		
		//Click OK
		click(byExamEndTimeOK);
		
		return this;
	} */
		
	//@Step ("Add Questions")
			
	@Step ("Save Exam")
	public addExamPage SaveExam () {
		
		click(bySaveExam);
		
		return this;
	}
	@Step ("Add Question")
	public addExamPage Add_Question (String Points, String Question) {
		
		click(byAddQuesbtn);
		
		writeText(byPoints,Points);
		
		writeText(byQuestion,Question);
		
		return this;
	}
	
	@Step ("Verify Total Score")
	public addExamPage Verify_Total_Score (String TScore) {
		
		assertEqually(byTScore,TScore);
		
		return this;
	}
	public ExamsPage Go_to_Exams_Template_List () {
		driver.get(ExamsTemplatePageURL);
		
		return new ExamsPage(driver);
	}
}

