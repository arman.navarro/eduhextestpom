package pages.write.add.Exams.Questions;

import org.openqa.selenium.WebDriver;
import io.qameta.allure.Step;
import pages.BasePage;

public class AddQuestionstoExam extends BasePage {

	
	public AddQuestionstoExam(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@Step("Click Add Question Button")
	public AddQuestionstoExam Add_Question_btn () {
		
		click(byAddQuesbtn);
		
		return this;
	}
	
	@Step("Fill in Question Points")
	public AddQuestionstoExam Fill_Question_Points (String Points) {
		writeText (byPoints, Points);
		
		return this;
	}
	
	@Step("Fill in Question")
	public AddQuestionstoExam Fill_Question_Identification (String IdentificationQuestion ) {
		writeText (byQuestion, IdentificationQuestion);
		return this;
	}
}
