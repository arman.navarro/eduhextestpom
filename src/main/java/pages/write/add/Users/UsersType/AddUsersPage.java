package pages.write.add.Users.UsersType;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import pages.BasePage;
import org.openqa.selenium.WebDriver;


public class AddUsersPage extends BasePage {
	//*********Constructor*********
    public AddUsersPage (WebDriver driver) {
        super(driver);
    }
    @Step("Verify Users Side Navigation: {0} step...")
    public AddUsersPage Usersbtn () {
    	click(UsersbtnBy);
    	
    	return this;
    }
    
    @Step("Verify Users All Side Navigation: {0} step...")
    public AddUsersPage UsersAllbtn () {
    	click(UsersAllbtnBy);
    	
    	return this;
    }

    @Step ("Verify Users Main view: {0} step...")
    public AddUsersPage VerifyUsersMainView (String expectedText) {
    	assertEquals(UsersMainViewTable, expectedText);
    	
    	return this;
    }

    @Step ("Click Add User: {0} step...")
    public AddUsersPage AddUserbtn () {
    	click(NewUserAddbtn);
    	
    	return this;
    }

    @Step ("Fill In User Information First Name: {0}, LastName: {1}, Username: {2}, Password: {3}"
    		+ " Email: {4}, Mobile Numer: {5}, method: {method} step... ")
    public AddUsersPage AddNewUserTeacher (
    		String FirstName, String LastName,
    		String Username, String Password,
    		String Email, String MobileNumber
    		) {
    	
    	//Enter FirstName
    	writeText(UsersFirstName, FirstName);
    	
    	//Enter LastName
    	writeText(UsersLastName, LastName);
    	
    	//Enter Username
    	writeText(UsersUsername, Username);
    	
    	//Enter Password
    	writeText(UsersPassword, Password);
    	
    	//Enter Email
    	writeText(UsersEmail, Email);
    	
    	//Enter Mobile Number
    	writeText(UsersMobileNum, MobileNumber);
    	
    	
    	return this;	
    }

    @Step ("Select Role for user: {0}, method: {method} step...")
    public AddUsersPage SelectRole () {
    	click(UsersRole);
    	
    	return this;
    }
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Users role are selectable")
    @Story("Test Verify Users role availability")
    @Step ("Assign User role: {0}, method: {method} step...")
    public AddUsersPage SelectRoleUsers () {
    	click(UsersRoleUsers);
    	return this;
    }
    
    @Step ("Click Outside: {0}, method: {method} step...")
    public AddUsersPage ClickOut1 () {
    	click(UsersClickOut);
    	
    	return this;
    }
    
    @Step ("Click Outside: {0}, method: {method} step...")
    public AddUsersPage ClickOut2 () {
    	click(UsersClickOut);
    	
    	return this;
    }

    @Step ("Select User type: {0}, method: {method} step...")
    public AddUsersPage SelectUsersType () {
    	click(UsersType);
    	
    	return this;
    }

    @Step ("Select User type as Teacher: {0}, method: {method} step...")
    public AddUsersPage SelectUsersTypeTeacher () {
    	click(UsersTypeTeacher);
    	
    	return this;
    }

    @Step("Click Save button: {0}, method: {method} step...")
    public AddUsersPage SaveUserbtn () {
    	click(UsersSavebtnBy);
    	
    	return this;
    }
    //Go to UsersPage
}
