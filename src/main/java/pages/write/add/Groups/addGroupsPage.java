package pages.write.add.Groups;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import pages.BasePage;


import org.openqa.selenium.WebDriver;

public class addGroupsPage extends BasePage {

	public addGroupsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	 @Step("Click Groups Side Navigation: {0} step...")
	    public addGroupsPage Groupsbtn () {
	    	click(GroupsbtnBy);
	    	
	    	return this;
	    }
	    
	    @Step ("Verify Groups Main view: {0} step...")
	    public addGroupsPage VerifyGroupsMainView (String expectedText) {
	    	assertEquals(GroupsMainViewTable, expectedText);
	    	
	    	return this;
	    }

	    @Step ("Click Add Group: {0} step...")
	    public addGroupsPage AddGroupsbtn () {
	    	click(NewGroupAddbtn);
	    	
	    	return this;
	    }
	    
	    @Step ("Fill in Group Name: {0}, Description: {1}, method: {method} step...")
	    public addGroupsPage Fill_In_GroupName_Description (String GroupName1, String Description1) {
	    	//Enter Group Name
	    	writeText(GroupName, GroupName1);
	    	//Enter Group Desc
	    	writeText(GroupDesc, Description1);
	    	
	    	return this;
	    	
	    }
	    
	    @Step ("Click Members Group: {0}, method: {method} step...")
	    public addGroupsPage Add_Members () {
	    	click(GroupMembers);
	    	
	    	return this;
	    }
	    
	    @Step ("Select Group Type: {0}, method: {method} step...")
	    public addGroupsPage Select_Group_Type_User () {
	    	click(GTypeUser);
	    	
	    	return this;
	    }

	    @Step ("Select Group Type: {0}, method: {method} step...")
	    public addGroupsPage Select_Group_Type_Student () {
	    	click(GTypeStudent);
	    	
	    	return this;
	    }
	    
	    @Step ("Select Group Type: {0}, method: {method} step...")
	    public addGroupsPage Select_Group_Type_Teacher () {
	    	click(GTypeTeacher);
	    	
	    	return this;
	    }
}
