package pages.write.assign.examTostudent;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import pages.BasePage;

import org.openqa.selenium.WebDriver;

public class AssignexamTostudent extends BasePage {

	public AssignexamTostudent(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	@Step("Verify Exams Side Navigation: {0} step...")
	public AssignexamTostudent Examsbtn () {
		click(ExamsbtnBy);
		
		return this;
	}
	
	@Step("Verify Exams Templates is Visible from the Side Navigation: {0} step...")
	public AssignexamTostudent Exams_Templatebtn () {
		click(ExamsTemplatebtnBy);
		
		return this;
	}
	

	@Step ("Verify Exams Main View: {0} step...")
	public AssignexamTostudent Verify_Exams_Main_View (String expectedText) {
		assertEquals(ExamsMainViewTable, expectedText);
		
		return this;
	}
	@Step("Navigate to Forward Page")
	public AssignexamTostudent Forward_Page () {
		click(byForwardPage);
		
		return this;
	}
}
