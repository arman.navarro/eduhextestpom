package pages.write.delete.Lessons;

import io.qameta.allure.Step;
import pages.BasePage;

import org.openqa.selenium.WebDriver;

public class deleteLessonsPage extends BasePage {

	public deleteLessonsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	@Step("Verify Lessons Side Navigation: {0} step...")
	public deleteLessonsPage Lessonsbtn () {
		click(LessonsbtnBy);
		
		return this;
	}

	@Step("Verify Lessons Main View: {0} step...")
	public deleteLessonsPage VerifyLessonsMainView (String expectedText) {
		assertEquals(LessonsMainViewTable, expectedText);
		
		return this;
	}
}
