package pages.write.delete.Classes;

import io.qameta.allure.Step;
import pages.BasePage;

import org.openqa.selenium.WebDriver;

public class deleteClassesPage extends BasePage {

	public deleteClassesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	@Step ("Verify Classes Side Navigation: {0} step...")
	public deleteClassesPage Classesbtn () {
		click(ClassesbtnBy);
		
		return this;	
	}

	@Step("Verify Classes Main view: {0} step...")
	public deleteClassesPage VerifyClassesMainView (String expectedText) {
		assertEquals(ClassesMainViewTable, expectedText);
		
		return this;
	}
	
	@Step("Verify the Added Class")
	public deleteClassesPage VerifyAddedClassFromMainView (String expectedText) {
		assertEquals(byClassesMainViewTable, expectedText);
		
		return this;
	}
	@Step("Delete a Class")
	public deleteClassesPage DeleteAClass () {
		click(byDeleteClassesbtn);
		
		return this;
	}
	
}
