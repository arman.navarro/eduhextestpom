package pages.write.delete.Exams;

import io.qameta.allure.Attachment;
import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class deleteExamsPage extends BasePage {

	public deleteExamsPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@Step("Verify Exams Side Navigation: {0} step...")
	public deleteExamsPage Examsbtn () {
		click(ExamsbtnBy);
		
		return this;
	}
	
	@Step("Verify Exams Templates is Visible from the Side Navigation: {0} step...")
	public deleteExamsPage Exams_Templatebtn () {
		click(ExamsTemplatebtnBy);
		
		return this;
	}
	

	@Step ("Verify Exams Main View: {0} step...")
	public deleteExamsPage Verify_Exams_Main_View (String expectedText) {
		assertEquals(ExamsMainViewTable, expectedText);
		
		return this;
	}
	
	@Step ("Verify the Added Exam")
	public deleteExamsPage VerifyAddedExamsFromMainView (String expectedText) {
		
		assertEquals(byExamsMainViewTable, expectedText);
		
		return this;
	}
	
	@Step ("Click Exam Options")
	public deleteExamsPage Exam_Options () {
		click(byEditExamTemp2);
		
		return this;
	}
	
	@Step("Delete an Exam")
	public deleteExamsPage DeleteAExam () {
		click(byDeleteExamsTempbtn);
		
		return this;
	}
	
}
