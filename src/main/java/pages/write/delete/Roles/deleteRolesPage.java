package pages.write.delete.Roles;

import org.openqa.selenium.WebDriver;
import io.qameta.allure.Step;
import pages.BasePage;

public class deleteRolesPage extends BasePage {

	public deleteRolesPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@Step("View Roles: {0} step..")
	public deleteRolesPage Rolesbtn ()
	{
		click(RolesbtnBy);
		return this;
	}

	@Step("Verify Roles Main view: {0} step...")
	public deleteRolesPage VerifyRolesMainView (String expectedText) {
		assertEquals(RolesMainViewTable, expectedText);
		
		return this;
	}
}
