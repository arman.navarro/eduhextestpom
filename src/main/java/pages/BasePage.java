package pages;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.openqa.selenium.interactions.Actions;

public class BasePage {

	public WebDriver driver;
    public WebDriverWait wait;
    private Actions action2 = new Actions(driver);

    //Constructor
    public BasePage (WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver,15);
    }

    //Wait Wrapper Method
    public void waitVisibility(By elementBy) {
        wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(elementBy));
    }
 
    //Click Method
    public void click (By elementBy) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).click();
    }

    //Write Text
    public void writeText (By elementBy, String text) {
        waitVisibility(elementBy);
        driver.findElement(elementBy).sendKeys(text);
    }
    //Wirte Number
    public void writeNum (By elementBy, CharSequence[] mobileNumber) {
    	waitVisibility(elementBy);
    	driver.findElement(elementBy).sendKeys(mobileNumber);
    }

    //Read Text
    public String readText (By elementBy) {
        waitVisibility(elementBy);
        return driver.findElement(elementBy).getText();
    }
    
    public void dragNdrop (WebElement elementBy, WebElement elementBy1) {
    	waitVisibility(elementBy);
    	getAction2().clickAndHold(elementBy).moveToElement(elementBy1).release(elementBy1).perform();
    
    }

    private void waitVisibility(WebElement elementBy) {
    	wait.until(ExpectedConditions.visibilityOfAllElements(elementBy));
		
	}

	//Assert
    public void assertEquals (By elementBy, String expectedText) {
        waitVisibility(elementBy);
        Assert.assertEquals(readText(elementBy), expectedText);
    }
    //Assert equally
    public void assertEqually (By elementBy, String expectedText) {
        waitVisibility(elementBy);
        Assert.assertEquals(getText(elementBy), expectedText);
    }
    //Get text
    public String getText (By elementBy) {
    	waitVisibility(elementBy);
    	return driver.findElement(elementBy).getText();
     

}
   
    //Login Page
    
    public Actions getAction2() {
		return action2;
	}

	public void setAction2(Actions action2) {
		this.action2 = action2;
	}
	
	protected WebElement usernameBy1 = (WebElement) By.id("username");
	protected WebElement passwordBy1 = (WebElement) By.id("username");
	protected By usernameBy = By.id("username");
    protected By passwordBy = By.id("pwd");
    protected By loginButtonBy = By.cssSelector("button.mx-auto");
    
    //Logout
    protected By logoutBy = By.cssSelector(".fa-sign-out-alt");
    
    //Side Navigation
    //Students
    protected By StudentsbtnBy = By.cssSelector(".fa-user-graduate");
    //Users btn 
	protected By UsersbtnBy = By.cssSelector(".fa-users"); 
	//Users all btn 
	protected By UsersAllbtnBy = By.cssSelector("span:contains('All')");
	//Teachers
	protected By TeachersbtnBy = By.cssSelector(".fa-chalkboard-teacher");
	//Groups
	protected By GroupsbtnBy = By.cssSelector(".fa-object-group");
	//Classes  
	protected By ClassesbtnBy = By.cssSelector(".fa-layer-group");
	//Exams
	protected By ExamsbtnBy = By.cssSelector(".fa-feather-alt");
  	protected By ExamsTemplatebtnBy = By.cssSelector(".fa-clone");
    //Lessons
  	protected By LessonsbtnBy = By.cssSelector(".fa-book");
  	//Roles
  	protected By RolesbtnBy = By.cssSelector(".fa-tags");
  	
  	
  	//Main View Table
  	//Students
  	protected By StudentsMainViewTable = By.cssSelector(".display-5");
  	//UsersAll
  	protected By UsersAllMainViewTable = By.cssSelector(".display-5");
  	protected By UsersMainViewTable = By.cssSelector(".display-5");
  	//Groups
  	protected By GroupsMainViewTable = By.cssSelector(".display-5");
  	//Teachers
  	protected By TeachersMainViewTable = By.cssSelector(".display-5");
  	//Classes
  	protected By ClassesMainViewTable = By.cssSelector(".display-5");
  	//Exams
  	protected By ExamsMainViewTable = By.cssSelector(".display-5");
  	//Lessons
  	protected By LessonsMainViewTable = By.cssSelector(".display-5");
  	//Roles
  	protected By RolesMainViewTable = By.cssSelector(".display-5");
  	
  	
  	
    //String URL's
    protected String studentsURL = "http://128.199.104.97:4200/class/list";
    protected String lesssonsURL = "http://128.199.104.97:4200/lesson/list";
  	protected String teachersURL = "128.199.104.97:4200/teacher/list";
    protected String classesURL = "http://128.199.104.97:4200/class/list";
  	protected String examsURL = "http://128.199.104.97:4200/exam/list";
  	protected String rolesURL = "128.199.104.97:4200/role/list";
  
   	//Pagination
  	protected By byPageNumber = By.cssSelector("select.form-control");
   	protected By byNextPage = By.cssSelector(".fa-caret-right");
   	protected By byForwardPage = By.cssSelector(".fa-fast-forward");
   
   	
   	//General Search
   	protected By SResult = By.cssSelector("div[tabindex=\"-1\"]");
   	//Search Classes Web Elements
   	protected By SearchBoxBy = By.cssSelector("input[placeholder=\"Search Classes\"]");
   	//Search Groups Web Elements
   	protected By SearchGroupsBy = By.cssSelector("input[placeholder=\"Search Groups\"]");
   	//Search Exams Web Elements
   	protected By SearchExamTemplatesBy = By.cssSelector("input[placeholder=\"Search Exam Templates\"]");
   	//Search Lessons Web Elements
   	protected By SearchLessonsBy = By.cssSelector("input[placeholder=\"Search Lessons\"]");
   	//Search Roles Web Elements
   	protected By SearchRolesBy = By.cssSelector("input[placeholder=\"Search Roles\"]");
   			
   	
   	//Add
   	//Add New User
   	protected By NewUserAddbtn = By.cssSelector(".float-right");
   	//Add New Groups
    protected By NewGroupAddbtn = By.cssSelector(".float-right");
    //Add New Roles
    protected By NewRolesAddbtn = By.cssSelector(".float-right");
    //Add New Class
    protected By NewClassAddbtn = By.cssSelector(".float-right");
   	
   													//Exam Add Buttons
   									// Add Exam Template btn
   									protected By byAddExamTemplatebtn = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/app-exam-list/div/div[2]/div[2]/button");
   									// Add Question
   									protected By byAddQuesbtn = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/app-exam-crud/div/div/div/button/span/h4");
   									
   													//Edit Buttons
   									//Edit Classes //Determined by div:nth-child
   									protected By byEditClassesbtn = By.cssSelector(".ag-center-cols-container > div:nth-child(2) > div:nth-child(3) > app-actions-cell:nth-child(1) > button:nth-child(1) > span:nth-child(1) > mat-icon:nth-child(1)");
   									//Edit Lessons
   									protected By byEditLessonsbtn = By.cssSelector(".ag-center-cols-container > div:nth-child(2) > div:nth-child(3) > app-actions-cell:nth-child(1) > button:nth-child(1) > span:nth-child(1) > mat-icon:nth-child(1)");
   									//Edit Roles
   									protected By byEditRolesbtn = By.cssSelector(".ag-center-cols-container > div:nth-child(2) > div:nth-child(3) > app-actions-cell:nth-child(1) > button:nth-child(1) > span:nth-child(1) > mat-icon:nth-child(1)");
   									//Edit Users
   									protected By byEditUsersbtn = By.cssSelector(".ag-center-cols-container > div:nth-child(2) > div:nth-child(5) > app-actions-cell:nth-child(1) > button:nth-child(1) > span:nth-child(1) > mat-icon:nth-child(1)");
   									//Edit Students
   									protected By byEditStudentsbtn = By.cssSelector("");
   									//Edit Teachers
   									protected By byEditTeachersbtn = By.cssSelector("");
   								
   													//Delete Buttons
   									//Delete Classes
   									protected By byDeleteClassesbtn = By.cssSelector("button.mat-warn:nth-child(1) > span:nth-child(1)");
   									//Delete Lessons
   									protected By byDeleteLessonsbtn = By.cssSelector("button.mat-warn:nth-child(1) > span:nth-child(1)");
   									//Delete Roles
   									protected By byDeleteRolesbtn = By.cssSelector("button.mat-warn:nth-child(1) > span:nth-child(1)");
   									//Delete Users
   									protected By byDeleteUsersbtn = By.cssSelector("button.mat-warn:nth-child(1) > span:nth-child(1)");
   									//Delete Students
   									protected By byDeleteStudentsbtn = By.cssSelector("button.mat-warn:nth-child(1) > span:nth-child(1)");
   									//Delete Teachers
   									protected By byDeleteTeachersbtn = By.cssSelector("button.mat-warn:nth-child(1) > span:nth-child(1)");
   									//Delete exams templates
   									protected By byDeleteExamsTempbtn = By.cssSelector("button.mat-warn:nth-child(1) > span:nth-child(1)");
   									
   									
   													//Edit Options
   									//Edit Options Users All //Determined by div:nth-child
   									protected By byEditUsersAll2 = By.cssSelector(".ag-center-cols-container > div:nth-child(2) > div:nth-child(5) > app-actions-cell:nth-child(1) > button:nth-child(2) > span:nth-child(1) > mat-icon:nth-child(1)");
   				
   									//Edit Options Exams Template
   									protected By byEditExamTemp2 = By.cssSelector(".ag-center-cols-container > div:nth-child(2) > div:nth-child(6) > app-actions-cell:nth-child(1) > button:nth-child(3) > span:nth-child(1) > mat-icon:nth-child(1)");
   									
   						//Table View Assertions
   					protected By byClassesMainViewTable = By.cssSelector("div[tabindex=\"-1\"]");
   					protected By byExamsMainViewTable = By.cssSelector("div[tabindex=\"-1\"]");
   					
   						//Assign Exam 
   					protected By byAssignExamtoStudent = By.cssSelector("");
   									
   									
   									
   									
			//Create Exam
			protected String myExamsURL = "http://128.199.104.97:4200/exam";
			protected String ExamsTemplatePageURL = "http://128.199.104.97:4200/exam/template/list";
			
			// Fill in exam information 
			//*[@id="mat-input-67"] 
	 
			protected By byExamName = By.cssSelector("input[placeholder=\"Exam Name\"]");  
			protected By byExamType = By.xpath("//*[@id=\"mat-select-0\"]/div/div[1]/span");
	
			// Exam type dropdown
			protected By byComprehensiveExam = By.xpath("/html/body/div[2]/div[2]/div/div/div/mat-option[1]/span");
			protected By byRecitationExam = By.xpath("/html/body/div[2]/div[2]/div/div/div/mat-option[2]/span");
			//By byExamTypeDropdown = By.xpath("//*[@id=\"mat-select-0\"]/div/div[1]/span");
		
		
			protected By byQuizExam = By.cssSelector("#mat-option-2 > span");
			protected By byPeriodicalExam = By.xpath("/html/body/div[2]/div[2]/div/div/div/mat-option[5]/span");
			protected By byParticipationExam = By.xpath("/html/body/div[2]/div[2]/div/div/div/mat-option[4]/span");
			protected By byFinalsExam = By.xpath("/html/body/div[2]/div[2]/div/div/div/mat-option[6]/span");
		
			//Exam Start Date
			protected By byExamStartDateBox = By.xpath("");
		
		
			//Exam Start Time
			protected By byExamStartTimeBox = By.xpath("//*[@id=\"mat-input-6\"]");
			protected By byExamStartTimeFirst = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/app-exam-crud/div/form/mat-form-field[8]/div/div[1]/div[3]/ngx-material-timepicker/div[2]/div/div/div[1]/div/ngx-material-timepicker-12-hours-face/ngx-material-timepicker-face/div/div/div[1]");
			protected By byExamStartTimeOK = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/app-exam-crud/div/form/mat-form-field[6]/div/div[1]/div[3]/ngx-material-timepicker/div[2]/div/div/div[2]/div[2]/ngx-material-timepicker-button/button/span");
		
			//Time Click out 
			protected By byTimeOut = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/app-exam-crud/div/form/mat-form-field[6]/div/div[1]/div[3]/ngx-material-timepicker/div[2]");
			//Times
		
			//Exam End Time
			protected By byExamEndTimeBox = By.xpath("//*[@id=\"mat-input-8\"]");
			protected By byExamEndTimeFirst = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/app-exam-crud/div/form/mat-form-field[8]/div/div[1]/div[3]/ngx-material-timepicker/div[2]/div/div/div[1]/div/ngx-material-timepicker-12-hours-face/ngx-material-timepicker-face/div/div/div[1]");
			protected By byExamEndTimeOK = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/app-exam-crud/div/form/mat-form-field[8]/div/div[1]/div[3]/ngx-material-timepicker/div[2]/div/div/div[2]/div[2]/ngx-material-timepicker-button/button/span");

			// Save exam 
			protected By bySaveExam = By.cssSelector("button.float-right:nth-child(1) > span:nth-child(1) > span:nth-child(1)");
			protected By byPoints = By.cssSelector("input[placeholder=\"Points\"]");
			protected By byQuestion = By.cssSelector("input[placeholder=\"Question\"]");
			protected By byTScore = By.cssSelector("input[type=\"Number\"]");
			
			// Delete Question
			protected By byDeleteQuestion = By.cssSelector("button.mt-2 > span:nth-child(1) > mat-icon:nth-child(1)");
			    
			    
			    
			    //Inside Add users
			    //By UsersAddbtn = By.cssSelector("");
			    protected By UsersSavebtnBy = By.cssSelector(".float-right > span:nth-child(1)");
			    //General
			    protected By UsersFirstName = By.cssSelector("input[placeholder=\"First Name\"]");
			    protected By UsersLastName = By.cssSelector("input[placeholder=\"Last Name\"]");			  
			    //Accessibility
			    protected By UsersUsername = By.cssSelector("input[placeholder=\"Username\"]");
			    protected By UsersPassword = By.cssSelector("input[placeholder=\"Password\"]");
			    //Role Dropdown
			    protected By UsersRole = By.cssSelector("span:contains('Role')");
			    //Role select User
			    protected By UsersRoleUsers = By.cssSelector("span:contains('USER')");
			    //UsersType Dropdown
			    protected By UsersType = By.cssSelector("input[placeholder\"User Types\"]");
			    //Click out
			    protected By UsersClickOut = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/user-crud/div/form/mat-accordion");
			    //UserType select Teacher
			    protected By UsersTypeTeacher = By.cssSelector("span:contains('Teacher')"); 
			    //Users Contact
			    protected By UsersEmail = By.cssSelector("input[placeholder=\"Email\"]");
			    protected By UsersMobileNum = By.cssSelector("input[placeholder=\"Mobile Number\"]");
			    
		//Create Class
			    
			    //Subject Name
			    protected By CSubjectName = By.cssSelector("input[placeholder=\"Subject Name\"]");
			    //Room Number
			    protected By CRoomNumber = By.cssSelector("input[placeholder=\"Room Number\"]");
			    
			    //Save Class
			    protected By SaveClass = By.cssSelector(".float-right > span:nth-child(1) > span:nth-child(1)");
			    
			    
			    
			    
			    	//Group Name
			    	protected By GroupName = By.cssSelector("input[palceholder=\"Group Name\"]");
			    	//Descirption
			    	protected By GroupDesc = By.cssSelector("input[placeholder=\"Description\"]");
					//Members
			    	protected By GroupMembers = By.cssSelector("input[placeholder=\"Pick One\"]");
			    	//Groups Type
			    	protected By GroupType = By.cssSelector("input[placeholder=\"Group Type\"]");
			    		//Group Type Classroom
			    		protected By GTypeClassroom = By.cssSelector("#mat-option-0 > span:nth-child(1)");
			    		//Group Type User
			    		protected By GTypeUser = By.cssSelector("#mat-option-0 > span:nth-child(2)");
			    		//Group Type Student
			    		protected By GTypeStudent = By.cssSelector("#mat-option-0 > span:nth-child(3)");
			    		//Group Type Teacher
			    		protected By GTypeTeacher = By.cssSelector("#mat-option-0 > span:nth-child(4)");
			    		
			    	
			    	/*
			    	 * protected By itemsInDropdown = By.cssSelector("");
			    	 
			    	//Dropdown Randomizer
			    	int size = itemsInDropdown.size();
			    	int randomNumber = ThreadLocalRandom.current().nextInt(0, size);
			    	itemsInDropdown.get(randomNumber).click();
			    	*/
			    	
			    	//Add Class
			    	
}
