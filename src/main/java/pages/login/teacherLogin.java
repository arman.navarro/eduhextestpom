package pages.login;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class teacherLogin extends BasePage {
	//*********Constructor*********
    public teacherLogin (WebDriver driver) {
        super(driver);
    }

    //*********Page Methods*********

    @Step ("Login Step with username: {0}, pwd: {1}, method: {method} step... ")
    public teacherLogin loginToEduhex (String username, String pwd){
        
    	//Enter Username(Email)
        writeText(usernameBy,username);
        
        //Enter Password
        writeText(passwordBy,pwd);
        
        //Click Login Button
        //click(loginButtonBy);
        return this;
    }
    @Step ("Click Login Button: {0}, method: {method} step...")
    public teacherLogin loginbtn () {
    	click(loginButtonBy);
    	
    	return this;
    }
}
