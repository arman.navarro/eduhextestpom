package pages.login;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class adminLogin extends BasePage {

	public adminLogin(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	//*********Page Methods*********

    @Step ("Login Step with username: {0}, pwd: {1}, method: {method} step... ")
    public adminLogin loginToEduhex (String username, String pwd){
        
    	//Enter Username(Email)
        writeText(usernameBy,username);
        
        //Enter Password
        writeText(passwordBy,pwd);
        
        //Click Login Button
        //click(loginButtonBy);
        return this;
    }
    @Step ("Click Login Button: {0}, method: {method} step...")
    public adminLogin loginbtn () {
    	click(loginButtonBy);
    	
    	return this;
    }
}
