package pages.login;

import io.qameta.allure.Step;
import pages.BasePage;
import org.openqa.selenium.WebDriver;

public class studentLogin extends BasePage {

	public studentLogin(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	@Step ("Login Step with username: {0}, pwd: {1}, method: {method} step... ")
    public studentLogin loginToEduhex (String username, String pwd){
        
    	//Enter Username(Email)
        writeText(usernameBy,username);
        
        //Enter Password
        writeText(passwordBy,pwd);
        
        //Click Login Button
        //click(loginButtonBy);
        return this;
    }
    @Step ("Click Login Button: {0}, method: {method} step...")
    public studentLogin loginbtn () {
    	click(loginButtonBy);
    	
    	return this;
    }

}
