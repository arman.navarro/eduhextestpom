package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage extends BasePage {
	//*********Constructor*********
    public DashboardPage (WebDriver driver) {
        super(driver);
    }

    //*********Page Variables*********
    String baseURLtest = "http://dev.eduhex.io/login";
    String baseURLlocal = "http://dev.eduhex.io/login";
    
    //*********Web Elements*********
    By signInButtonBy = By.xpath("/html/body/app-root/mat-drawer-container/mat-drawer-content/div/app-login/div/div/div[2]/button[2]/span");


    //*********Page Methods*********
    //Go to DashboardPage
    public DashboardPage goToDashboardPage (){
        driver.get(baseURLtest);
        return this;
    }

    
    //Go to LoginPage
    public LoginPage goToLoginPage (){
        driver.get(baseURLtest);
        return new LoginPage(driver);
        
}
}
