package pages;


import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;


public class LoginPage extends BasePage {
	
	//*********Constructor*********
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    //*********Page Methods*********

    @Step ("Login Step with username: {0}, pwd: {1}, method: {method} step... ")
    public LoginPage loginToEduhex (String username, String pwd) {
        
    	//Enter Username(Email)
        writeText(usernameBy,username);
        
        //Enter Password
        writeText(passwordBy,pwd);
        
        //Click Login Button
        //click(loginButtonBy);
        return this;
    }
    @Step ("Click Login Button: {0}, method: {method} step...")
    public LoginPage loginbtn () {
    	click(loginButtonBy);
    	
    	return this;
    }

    //Verify Username Condition
    //public LoginPage verifyLoginUserName (String expectedText) {
       // assertEquals(errorMessageUsernameBy, expectedText);
       // return this;
   // }

    //Verify Password Condition
   // public LoginPage verifyLoginPassword (String expectedText) {
      //  assertEquals(errorMessagePasswordBy, expectedText);
       // return this;
//}

}
