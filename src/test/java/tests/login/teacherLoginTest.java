package tests.login;

import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import tests.BaseTest;

@Epic ("Teacher Login credentials")
@Feature("Login")

public class teacherLoginTest extends BaseTest implements IHookable {
	
	 @Severity(SeverityLevel.BLOCKER)
	    @Description("Login: Teacher Credentials")
	    @Story("Test Verify Valid Login")
		@Test (priority = 0)
	    public void Teacher_Valid_Login_Valid_UserName_and_Password () {

	        //*************PAGE INSTANTIATIONS*************
	        DashboardPage DashboardPage = new DashboardPage(driver);

	        //*************PAGE METHODS********************
	        System.out.println("Login to EduHex via Teacher");
	        DashboardPage.goToDashboardPage()
	                .goToLoginPage()
	                .loginToEduhex("Teacher", "user")
	        		.loginbtn();
	                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
	                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
	      
	    }

}
