package tests.login;

import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import tests.BaseTest;

@Epic ("Admin Login credentials")
@Feature("Login")

public class adminLoginTest extends BaseTest implements IHookable {
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Login: Admin Credentials")
    @Story("Test Verify Valid Login")
	@Test (priority = 0)
    public void Student_Valid_Login_Valid_UserName_and_Password () {

        //*************PAGE INSTANTIATIONS*************
        DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        System.out.println("Login to EduHex via Admin");
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("admin", "user")
        		.loginbtn();
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
      
    }
}
