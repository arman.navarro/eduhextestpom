package tests.write.add.Users.UsersType;


import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.write.add.Users.UsersType.AddUsersPage;
import tests.BaseTest;

@Epic("Add New Users Functionality")
@Feature("Add Users")
public class addUsersTest extends BaseTest {

	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 1)
	public void Valid_Login_Valid_Username_and_Password () {
		//*************PAGE INSTANTIATIONS*************
		DashboardPage DashboardPage = new DashboardPage(driver);
		
		
		//*************PAGE METHODS********************
		System.out.println("Login to Eduhex");
		DashboardPage.goToDashboardPage()
		.goToLoginPage()
		.loginToEduhex("admin", "user")
		.loginbtn();
	}
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Users Side Navigation Clickable")
    @Story("Test Verify Users Main View")
	@Test (priority = 2, dependsOnMethods="Valid_Login_Valid_Username_and_Password")
	public void Navigate_to_Users_List () {
		//*************PAGE INSTANTIATIONS*************
		AddUsersPage AddUsersPage = new AddUsersPage(driver);
		
		//*************PAGE METHODS********************
		System.out.println("Navigate to Users: ALL");
		AddUsersPage.Usersbtn()
		.UsersAllbtn();
	
	}
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Users Main View is avalable")
    @Story("Test Verify Users Main View")
	@Test (priority = 3, dependsOnMethods="Navigate_to_Users_List")
	public void Verify_Users_Main_View () {
		//Page instantiations
		AddUsersPage AddUsersPage = new AddUsersPage(driver);
		
		//Page Methods
		System.out.println("Verifying Users Main view");
		AddUsersPage.VerifyUsersMainView("Users");
		}
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Add New User button is clickable")
    @Story("Test Verify Add New User button is present")
	@Test (priority = 4, dependsOnMethods="Verify_Users_Main_View")
	public void Verify_Add_User_Button_is_Present () {
		//Page Instantiations
		AddUsersPage AddUsersPage = new AddUsersPage(driver);
	
		//Page Methods
		System.out.println("Verifying Add User button is Present");
		AddUsersPage.AddUserbtn();
	}
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Adding users information")
    @Story("Test Verify Add new user page")
	@Test (priority = 5, dependsOnMethods="Verify_Add_User_Button_is_Present")
	public void Fill_Users_Information () {
		//Page instantations
		AddUsersPage AddUsersPage = new AddUsersPage(driver);
	
		//Page Methods
		System.out.println("Adding New user Information");
		AddUsersPage.AddNewUserTeacher ("Juan","Dela Cruz","jdcruz","jdcruz","arman.navarro@teravibe.com","09123456789");
	
	}
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify options for Users role are available")
    @Story("Test Verify Users role options")
	@Test (priority = 6, dependsOnMethods="Fill_Users_Information")
	public void Select_Role_Users_Dropdown () {
		//Page Instantations
		AddUsersPage AddUsersPage = new AddUsersPage(driver);
	
		//Page Methods
		System.out.println("Selecting User Role");
		AddUsersPage.SelectRole();
				
				
	}
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify options for Users role are available")
    @Story("Test Verify Users role options")
    @Test(priority = 7, dependsOnMethods="Select_Role_Users_Dropdown")
    public void Select_A_Role ( ) {
    	//Page instantations
    	AddUsersPage AddUsersPage = new AddUsersPage(driver);
    	
    	//Page methods
    	System.out.println("Selecting User Roles: User");
    	AddUsersPage.SelectRoleUsers();
    }
    
/**
	@Test (priority = 7, dependsOnMethods="Select_Role_Users")
	public void ClickOut1 () {
		//Page Instantations
		AddUsersPage AddUsersPage = new AddUsersPage(driver);
	
		//Page Methods
		AddUsersPage.ClickOut1();
			
	}
*/
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Options for User type are available")
    @Story("Test Verify Users type options")
	@Test (priority = 9, dependsOnMethods="Select_A_Role")
	public void Select_User_Type_Dropdown () {
		//Page instantations
		AddUsersPage AddUsersPage = new AddUsersPage(driver);
	
		//Page Methods
		System.out.println("Selecting Users Type");
		AddUsersPage.SelectUsersType();
				
} 
    
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify User type dropdown is selectable")
    @Story("Test Verify User type availability")
    @Test(priority = 10, dependsOnMethods="Select_User_Type_Dropdown")
    public void Select_A_User_Type () {
    	//Page instantations
    	AddUsersPage AddUsersPage = new AddUsersPage(driver);
    	
    	//Page Methods
    	System.out.println("Selecting Users Type: Teacher");
    	AddUsersPage.SelectUsersTypeTeacher();
    }
    
    /**
	@Test (priority = 8, dependsOnMethods="Select_User_Type")
	public void ClickOut2 () {
		//Page Instantations
	AddUsersPage AddUsersPage = new AddUsersPage(driver);
	
		//Page Methods
	AddUsersPage.ClickOut2();
			
	}
*/
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Save new User button is Present")
    @Story("Test Verify Save new User button is Clickable after all required fields are filled")
	@Test (priority = 10, dependsOnMethods="Select_A_User_Type")
	public void Save_New_User ( ) {
		//Page instantations
	AddUsersPage AddUsersPage = new AddUsersPage(driver);
	
		//Page Methods
	System.out.println("Saving New User");
	AddUsersPage.SaveUserbtn();
	}
}