package tests.write.add.Exams.Questions;

import org.openqa.selenium.interactions.Actions;
import org.testng.IHookable;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.write.add.Exams.*;
import tests.BaseTest;
import pages.write.add.Exams.Questions.*;

@Epic("Add Question to Exam")
@Feature("Adding Questions")

public class AddQuestionstoExamTest extends BaseTest implements IHookable {
	@Severity(SeverityLevel.BLOCKER)
    @Description("Login: Teacher Credentials")
    @Story("Test Verify Valid Login")
	@Test (priority = 0)
    public void Teacher_Valid_Login_Valid_UserName_and_Password () {

        //*************PAGE INSTANTIATIONS*************
        DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        System.out.println("Login to Eduhex");
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("admin", "user")
        		.loginbtn();
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
      
    }

	@Severity(SeverityLevel.BLOCKER)
	@Description("Verify My Exams Side Navigation is Clickable")
	@Story("Test Verify My Exams Main View")
	@Test (priority =1)
	public void Navigate_to_My_Exams_Side_Nav () {
		//Page instantations
		addExamPage addExamPage = new addExamPage(driver);
		
		//Page methods
		System.out.println("Click Exams");
		addExamPage.myExamsbtn();
	}
	
	@Severity(SeverityLevel.CRITICAL)
	@Description("Test Verify Exams Template Main View")
	@Story("Test Verify Exams Template Main View")
	@Test(priority = 2)
	public void Navigate_to_My_Exams_Template_view () {
		//Page instantations
	addExamPage addExamPage = new addExamPage(driver);
		//Page methods
	System.out.println("Click Exams Template");
	addExamPage.myExamsTemplatebtn();
		
	}
	
	@Severity(SeverityLevel.CRITICAL)
	@Description("Test Verify Exam Template Main View")
	@Story("Test Verify Exam Template Main View")
	@Test(priority = 3)
	public void Verify_My_Exam_Template_Main_View () {
		//PAge instantations
		addExamPage addExamPage = new addExamPage(driver);
		
		//Page methods
		System.out.println("Verifying Exam Template Main View");
		addExamPage.Verify_Exam_Template_Main_View("Exam Templates");
	}
	
	@Severity(SeverityLevel.BLOCKER)
	@Description("Verify Add Exam button is clickable")
	@Story("Test Verify Add Exam button is present")
	@Test(priority = 4)
	public void Verify_Add_Exam_Button_is_Present () {
		//Page instantations
		addExamPage addExamPage = new addExamPage(driver);
		
		//Page methods
		System.out.println("Verifying Add Exam Template button is present");
		addExamPage.AddExambtn();
	
	}
	
	@Severity(SeverityLevel.BLOCKER)
	@Description("Filling Exam Name")
	@Story("Test Verify Add Exam Page")
	@Test(priority = 5)
	public void  Fill_In_Exam_Name () {
		//Page instantations
		addExamPage addExamPage = new addExamPage(driver);
		
		//Page methods
		System.out.println("Fill in Exam Name");
		addExamPage.Fill_in_Exam_Name("Exam no.1");
	}
	
	@Severity(SeverityLevel.BLOCKER)
	@Description("Selecting Exam Type: Quiz")
	@Story("Test Verify Add Exam Page")
	@Test(priority = 6)
	public void Fill_in_Exams_Type () {
		//Page instantations
		addExamPage addExamPage = new addExamPage(driver);
		
		//Page methods
		System.out.println("Fill in Exam Type");
		addExamPage.Fill_in_Exam_type()
		.Select_Exam_Type_Quiz();
	}

	
	@Severity(SeverityLevel.BLOCKER)
	@Description("Assign Exam Start Time")
	@Story("Exam Start Time")
	@Test(priority = 7)
	public void Select_Exam_StartTime () {
		//Page instantations
		addExamPage addExamPage = new addExamPage(driver);
		Actions act = new Actions(driver);
		
		//Page methods
		System.out.println("Select Exam Start time");
		addExamPage.Select_Exam_Start_Time_1();
		
	}
	
	@Severity(SeverityLevel.BLOCKER)
	@Description("Assign Exam End Time")
	@Story("Exam End Time")
	@Test(priority = 8)
	public void Select_Exam_EndTime () {
		//Page instantations
		addExamPage addExamPage = new addExamPage(driver);
		Actions act = new Actions(driver);
		
		//Page methods
		System.out.println("Select Exam End Time");
		//act.moveToElement(driver.findElement(By.xpath("//*[@id=\"mat-input-6\"]"))).sendKeys("10:00 am");
		addExamPage.Select_Exam_End_Time_1();
	}
	
	@Severity(SeverityLevel.BLOCKER)
	@Description("Verify/Click Add Question Button")
	@Story("Adding Questions")
	@Test(priority =9)
	public void Add_Question_Btn () {
		//Page instantations
		AddQuestionstoExam AddQuestionstoExam = new AddQuestionstoExam(driver);
		
		//Page methods
		System.out.println("Click Add Question button");
		AddQuestionstoExam.Add_Question_btn();
	}
	@Severity(SeverityLevel.CRITICAL)
	@Description("Add Question Points to 3")
	@Story("Exam Points")
	@Test(priority =10)
	public void Add_Question_Points () {
		//Page instantations
		AddQuestionstoExam AddQuestionstoExam = new AddQuestionstoExam(driver);
		
		//Page methods
		System.out.println("Fill in Exam Points");
		AddQuestionstoExam.Fill_Question_Points("2");
	}
	
	@Severity(SeverityLevel.CRITICAL)
	@Description("Add Questions")
	@Story("Exam Questions")
	@Test(priority =11)
	public void Add_Question_1 () {
		//Page instantations
		AddQuestionstoExam AddQuestionstoExam = new AddQuestionstoExam(driver);
		
		//Page methods
		System.out.println("Fill Question Type: Identification");
		AddQuestionstoExam.Fill_Question_Identification("Question 1");
	}
	
	@Severity(SeverityLevel.BLOCKER)
	@Description("Save Exam")
	@Story("Save Exam")
	@Test(priority = 12)
	public void Save_Exam () {
		//Page instantations
		addExamPage addExamPage = new addExamPage(driver);
		//Page methods
		System.out.println("Saving Exam");
		addExamPage.SaveExam();
	}

}
