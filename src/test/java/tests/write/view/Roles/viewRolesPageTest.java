package tests.write.view.Roles;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.IHookable;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.feature.roles.RolesPage;
import pages.write.add.Exams.*;
import tests.BaseTest;
import pages.write.add.Exams.Questions.*;
import pages.write.view.Roles.viewRolesPage;

@Epic("View Role")
@Feature("Random View")

public class viewRolesPageTest extends BaseTest implements IHookable {
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 0)
	public void Valid_Login_Valid_Username_and_Password () {
		//*************PAGE INSTANTIATIONS*************
		DashboardPage DashboardPage = new DashboardPage(driver);
		
		
		//*************PAGE METHODS********************
		System.out.println("Login to EduHex");
		DashboardPage.goToDashboardPage()
		.goToLoginPage()
		.loginToEduhex("admin", "user")
		.loginbtn();
	}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Roles Navigation label is Present")
    @Story("Test Verify Roles Navigation is Clickable")
	@Test (priority = 1)
	public void Navigate_to_Roles_List () {
		//*************PAGE INSTANTIATIONS*************
		RolesPage RolesPage = new RolesPage(driver);
		
		//*************PAGE METHODS********************
		System.out.println("Navigate to Roles");
		RolesPage.Rolesbtn();
		}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Roles Main view is viewable")
    @Story("Test Verify Roles Main view")
	@Test (priority = 2)
	public void Verify_Roles_Main_View () {
		//Page instantiations
		RolesPage RolesPage = new RolesPage(driver);
		
		//Page Methods
		System.out.println("Verifying Roles Main View");
		RolesPage.VerifyRolesMainView("Roles");
	}
	
	@Severity(SeverityLevel.CRITICAL)
    @Description("View a Role")
    @Story("Test Role View")
	@Test (priority = 2)
	public void View_a_Role () {
		//Page instantiations
		viewRolesPage viewRolesPage = new viewRolesPage(driver);
		
		//Page Methods
		viewRolesPage.View_a_Role();
	}
}
