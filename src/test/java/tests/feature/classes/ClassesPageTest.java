package tests.feature.classes;

import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import tests.BaseTest;
import pages.feature.classes.ClassesPage;

@Epic("Classes Main View and Navigation")
@Feature("Classes")
public class ClassesPageTest extends BaseTest implements IHookable {
	
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test (priority = 0)
	public void ValidLoginTest_ValidUsernamePassword () {
	//*************PAGE INSTANTIATIONS*************
	DashboardPage DashboardPage = new DashboardPage(driver);
	
	
	//*************PAGE METHODS********************
	System.out.println("Login as Admin");
	DashboardPage.goToDashboardPage()
	.goToLoginPage()
	.loginToEduhex("admin", "user")
	.loginbtn();
}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Classes  Side Navigation is  Present")
    @Story("Test Verify Classes Main View")
	@Test (priority = 1)
	public void Navigate_to_Classes_List () {
		//Page instantiations
		ClassesPage ClassesPage = new ClassesPage(driver);
		
		
		//Page Methods
		System.out.println("Navigate to Classes List");
		ClassesPage.Classesbtn();
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Classes Main view is available")
    @Story("Test Verify Classes Main View")
	@Test(priority = 2)
	public void Verify_Classes_Main_View () {
		//Page instantations
		
		ClassesPage ClassesPage = new ClassesPage(driver);
		
		//Page Methods
		System.out.println("Verify Classes Main View");
		ClassesPage.VerifyClassesMainView("Classes");
	}
}
