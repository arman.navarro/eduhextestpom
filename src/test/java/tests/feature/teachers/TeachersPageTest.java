package tests.feature.teachers;

import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import tests.BaseTest;
import pages.feature.teachers.TeachersPage;

@Epic("Teachers Main View and Navigation")
@Feature("Teachers")
public class TeachersPageTest extends BaseTest implements IHookable {
	
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 0)
	public void Valid_Login_Valid_Username_and_Password () {
		//*************PAGE INSTANTIATIONS*************
		DashboardPage DashboardPage = new DashboardPage(driver);
		
		
		//*************PAGE METHODS********************
		System.out.println("Login to EduHex");
		DashboardPage.goToDashboardPage()
		.goToLoginPage()
		.loginToEduhex("admin", "user")
		.loginbtn();
	}
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Users Side Navigation section is Clickable")
    @Story("Test Users Side Navigation label is Present")
	@Test(priority = 1)
	public void Navigate_to_Users () {
	//Page instantiations
	TeachersPage TeachersPage = new TeachersPage(driver);
	
	//Page Methods
	System.out.println("Navigate to Users");
	TeachersPage.Users();
	}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Teachers Side Navigation section is Clickable")
    @Story("Test Teachers Side Navigation label is Present")
	@Test(priority = 2)
	public void Navigate_to_Teachers_List () {
	//Page instantiations
		System.out.println("Navigate to Users: Teachers");
	TeachersPage TeachersPage = new TeachersPage(driver);
	
	//Page Methods
	TeachersPage.Teachersbtn();
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Teachers Main View table is Viewable")
    @Story("Test Verify Teachers Main View table")
	@Test(priority = 3)
	public void Verify_Teachers_Main_View () {
		//Page instantiations
		TeachersPage TeachersPage = new TeachersPage(driver);
		
		//Page Methods
		System.out.println("Verifying Teachers Main View");
		TeachersPage.VerifyTeachersMainView("Teachers");
	}

}
