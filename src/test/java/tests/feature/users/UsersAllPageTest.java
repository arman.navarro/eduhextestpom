package tests.feature.users;

import org.testng.IHookable;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.feature.users.UsersAllPage;
import tests.BaseTest;

@Epic("Users Main View and Navigation")
@Feature("Users")
public class UsersAllPageTest extends BaseTest implements IHookable {

    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 0)
	public void Valid_Login_Valid_Username_and_Password () {
		//*************PAGE INSTANTIATIONS*************
		DashboardPage DashboardPage = new DashboardPage(driver);
		
		
		//*************PAGE METHODS********************
		DashboardPage.goToDashboardPage()
		.goToLoginPage()
		.loginToEduhex("admin", "user")
		.loginbtn();
	}
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Users Side Navigation Clickable")
    @Story("Test Verify Users Main View")
	@Test (priority = 1)
	public void Navigate_to_Users () {
		//*************PAGE INSTANTIATIONS*************
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//*************PAGE METHODS********************
		UsersAllPage.Usersbtn();
	
	}
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Users: ALL Side Navigation Clickable")
    @Story("Test Verify Users Main View")
	@Test (priority = 2)
	public void Navigate_to_Users_All () {
		//*************PAGE INSTANTIATIONS*************
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//*************PAGE METHODS********************
		UsersAllPage.UsersAllbtn();
	
	}
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Users Main View is avalable")
    @Story("Test Verify Users Main View")
	@Test (priority = 3)
	public void Verify_Users_Main_View () {
		//PAge instantiations
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//Page Methods
		UsersAllPage.VerifyUsersMainView("Users");
		}
	}
	/**
	@Test (priority = 4, dependsOnMethods="Verify_UsersMainView")
	public void Verify_AddUser () {
		//Page Instantiations
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//Page Methods
		UsersAllPage.AddUserbtn();
	}
	
	@Test (priority = 5, dependsOnMethods="Verify_AddUser")
	public void Fill_UsersInformation () {
		//Page instantations
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//Page Methods
		UsersAllPage.AddNewUserTeacher ("Juan","Dela Cruz","jdcruz","jdcruz","arman.navarro@teravibe.com","09123456789");
		
	}
	
	@Test (priority = 6, dependsOnMethods="Fill_UsersInformation")
	public void SelectRoleUsers () {
		//Page Instantations
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//Page Methods
		UsersAllPage.SelectRole()
				 .SelectRoleUsers()
				 .ClickOut1();
	}
	
	//@Test (priority = 7, dependsOnMethods="SelectRoleUsers")
	//public void ClickOut1 () {
		//Page Instantations
		//UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//Page Methods
		//UsersAllPage.ClickOut1();
				
	//}
	
	@Test (priority = 8, dependsOnMethods="SelectRoleUsers")
	public void SelectUserType () {
		//Page instantations
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//Page Methods
		UsersAllPage.SelectUsersType()
				.SelectUsersTypeTeacher()
				.ClickOut2();
	}
	//@Test (priority = 8, dependsOnMethods="SelectUserType")
	//public void ClickOut2 () {
		//Page Instantations
	//	UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//Page Methods
	//	UsersAllPage.ClickOut2();
				
	//}
	
	@Test (priority = 9, dependsOnMethods="SelectUserType")
	public void SaveNewUser ( ) {
	//Page instantations
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//Page Methods
		UsersAllPage.SaveUserbtn();
	}
}
*/
