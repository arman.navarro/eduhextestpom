package tests.feature.users;

import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.feature.users.UsersAllPage;
import pages.feature.users.UsersTeachersPage;
import tests.BaseTest;

@Epic("Users-Teachers Main View and Navigation")
@Feature("Users-Teachers")
public class UsersTeachersPageTest extends BaseTest {

	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 0)
	public void Valid_Login_Valid_Username_and_Password () {
		//*************PAGE INSTANTIATIONS*************
		DashboardPage DashboardPage = new DashboardPage(driver);
		
		
		//*************PAGE METHODS********************
		DashboardPage.goToDashboardPage()
		.goToLoginPage()
		.loginToEduhex("admin", "user")
		.loginbtn();
	}
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Users Side Navigation Clickable")
    @Story("Test Verify Users Main View")
	@Test (priority = 1)
	public void Navigate_to_Users () {
		//*************PAGE INSTANTIATIONS*************
		UsersAllPage UsersAllPage = new UsersAllPage(driver);
		
		//*************PAGE METHODS********************
		UsersAllPage.UsersAllbtn();
	
	}
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Users: Teachers Side Navigation Clickable")
    @Story("Test Verify Teachers Main View")
	@Test (priority = 2)
	public void Navigate_to_Users_Teachers () {
		//*************PAGE INSTANTIATIONS*************
		UsersTeachersPage UsersTeachersPage = new UsersTeachersPage(driver);
		
		//*************PAGE METHODS********************
		UsersTeachersPage.UsersTeacherbtn();
		
	}
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Teachers Main View is avalable")
    @Story("Test Verify Teachers Main View")
	@Test (priority = 3)
	public void Verify_Teachers_Main_View () {
		//PAge instantiations
    	UsersTeachersPage UsersTeachersPage = new UsersTeachersPage(driver);
    	
		
		//Page Methods
    	UsersTeachersPage.UsersTeachersMainView("TEACHERS");
	}
}
