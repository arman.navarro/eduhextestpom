package tests.feature.search.searchExamsPageTest;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.IHookable;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.write.add.Exams.addExamPage;
import pages.feature.exams.ExamsPage;
import pages.feature.search.searchExamsPage.searchExamsPage;
import tests.BaseTest;

@Epic("Search Exams")
@Feature("Search")

public class searchExamsPageTest extends BaseTest implements IHookable {
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test (priority = 0)
	public void Teacher_Valid_Login_Valid_UserName_and_Password () {

        //*************PAGE INSTANTIATIONS*************
        DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        System.out.println("Login to Eduhex");
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("admin", "user")
        		.loginbtn();
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
      
    }
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Exam  Side Navigation is  Present")
    @Story("Test Verify Exam Main View")
	@Test (priority = 1)
	public void Navigate_to_Exams () {
		
		//Page instantiations
		ExamsPage ExamsPage = new ExamsPage(driver);
				
				
		//Page Methods
		System.out.println("Click Exams");
		ExamsPage.Examsbtn();
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Exams Template Side Navigation")
    @Story("Test Verify Exams Template Side Navigation")
	@Test(priority = 2)
	public void Navigate_to_Exams_Template_View () {
		//Page instantations
		
		searchExamsPage searchExamsPage = new searchExamsPage(driver);
		
		//Page Methods
		System.out.println("Click Exams Template");
		searchExamsPage.myExamsTemplatebtn();
	}
	@Severity(SeverityLevel.CRITICAL)
	@Description("Test Verify Exam Template Main View")
	@Story("Test Verify Exam Template Main View")
	@Test(priority = 3)
	public void Verify_My_Exam_Template_Main_View () {
		//PAge instantations
		searchExamsPage searchExamsPage = new searchExamsPage(driver);
		
		//Page methods
		System.out.println("Verify Exams Template Main View");
		searchExamsPage.Verify_Exam_Template_Main_View("Exam Templates");
	}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Input text to Search box")
    @Story("Search Exam")
	@Test (priority = 4)
	public void Perform_Search_Exam () {
		
		//Page instantations
		searchExamsPage searchExamsPage = new searchExamsPage(driver);
		//Page methods
		System.out.println("Search Exam: SCI-101 COMPS");
		searchExamsPage.Search_a_Exam("SCI-101 COMPS");
	}
	
	
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Search Results")
    @Story("Search Results")
	@Test (priority = 5)
	public void Verify_Search_Results () {
		//Page instantations
		searchExamsPage searchExamsPage = new searchExamsPage(driver);
		//Page Methods
		System.out.println("Verifying Search Results");
		searchExamsPage.Search_Result("SCI-101 COMPS");
	}

}
