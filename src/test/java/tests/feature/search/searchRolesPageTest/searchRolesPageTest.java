package tests.feature.search.searchRolesPageTest;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.IHookable;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.feature.roles.*;
import pages.feature.search.searchRolesPage.searchRolesPage;
import tests.BaseTest;

@Epic("Search Roles")
@Feature("Search")



public class searchRolesPageTest extends BaseTest implements IHookable {
	@Severity(SeverityLevel.BLOCKER)
    @Description("")
    @Story("")
	@Test (priority = 0)
	public void Teacher_Valid_Login_Valid_UserName_and_Password () {

        //*************PAGE INSTANTIATIONS*************
        DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        System.out.println("Login to Eduhex");
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("admin", "user")
        		.loginbtn();
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
      
    }

	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Roles  Side Navigation is  Present")
    @Story("Test Verify Roles Main View")
	@Test (priority = 1)
	public void Navigate_to_Roles_List () {
		
		//Page instantiations
		RolesPage RolesPage = new RolesPage(driver);
				
				
		//Page Methods
		System.out.println("Navigate to Roles");
		RolesPage.Rolesbtn();
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Roles Main view is available")
    @Story("Test Verify Roles Main View")
	@Test(priority = 2)
	public void Verify_Roles_Main_View () {
		//Page instantations
		
		RolesPage RolesPage = new RolesPage(driver);
		
		//Page Methods
		System.out.println("Verify Roles Main View");
		RolesPage.VerifyRolesMainView("Roles");
	}

	@Severity(SeverityLevel.BLOCKER)
    @Description("Input text to Search box")
    @Story("Search Roles")
	@Test (priority = 3)
	public void Perform_Search_Roles () {
		
		//Page instantations
		searchRolesPage searchRolesPage = new searchRolesPage(driver);
		//Page methods
		System.out.println("Perform a Search: USER");
		searchRolesPage.Search_a_Role("USER");
	}
	
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Search Results")
    @Story("Search Results")
	@Test (priority = 4)
	public void Verify_Search_Results () {
		//Page instantations
		searchRolesPage searchRolesPage = new searchRolesPage(driver);
		//Page Methods
		System.out.println("Verify Search Results");
		searchRolesPage.Search_Result("USER");
	}


}
