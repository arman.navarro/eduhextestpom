package tests.feature.search.searchClassesPageTest;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.IHookable;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.feature.classes.ClassesPage;
import tests.BaseTest;
import pages.feature.search.searchClassesPage.*;

@Epic("Search Classes")
@Feature("Search")

public class searchClassesPageTest extends BaseTest implements IHookable {

	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test (priority = 0)
	public void Teacher_Valid_Login_Valid_UserName_and_Password () {

        //*************PAGE INSTANTIATIONS*************
        DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        System.out.println("Login to Eduhex");
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("admin", "user")
        		.loginbtn();
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
      
    }
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Classes  Side Navigation is  Present")
    @Story("Test Verify Classes Main View")
	@Test (priority = 1)
	public void Navigate_to_Classes_List () {
		
		//Page instantiations
		ClassesPage ClassesPage = new ClassesPage(driver);
				
				
		//Page Methods
		System.out.println("Navigate to Classes");
		ClassesPage.Classesbtn();
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Classes Main view is available")
    @Story("Test Verify Classes Main View")
	@Test(priority = 2)
	public void Verify_Classes_Main_View () {
		//Page instantations
		
		ClassesPage ClassesPage = new ClassesPage(driver);
		
		//Page Methods
		System.out.println("Verify Classes Main View");
		ClassesPage.VerifyClassesMainView("Classes");
	}

	@Severity(SeverityLevel.BLOCKER)
    @Description("Input text to Search box")
    @Story("Search Classes")
	@Test (priority = 3)
	public void Perform_Search_Class () {
		
		//Page instantations
		searchClassesPage searchClassesPage = new searchClassesPage(driver);
		//Page methods
		System.out.println("Perform a Search: MATH 101");
		searchClassesPage.Search_a_Class("MATH 101");
	}
	
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Search Results")
    @Story("Search Results")
	@Test (priority = 4)
	public void Verify_Search_Results () {
		//Page instantations
		searchClassesPage searchClassesPage = new searchClassesPage(driver);
		//Page Methods
		System.out.println("Verify Search Results");
		searchClassesPage.Search_Result("MATH 101");
	}

}
