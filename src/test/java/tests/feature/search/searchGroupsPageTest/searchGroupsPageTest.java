package tests.feature.search.searchGroupsPageTest;

import org.testng.IHookable;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.feature.groups.GroupsPage;
import pages.feature.search.searchGroupsPage.searchGroupsPage;
import tests.BaseTest;

@Epic("Search Groups")
@Feature("Search")

public class searchGroupsPageTest extends BaseTest implements IHookable {
	@Severity(SeverityLevel.BLOCKER)
    @Description("")
    @Story("")
	@Test (priority = 0)
	public void Teacher_Valid_Login_Valid_UserName_and_Password () {

        //*************PAGE INSTANTIATIONS*************
        DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        System.out.println("Login to Eduhex");
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("admin", "user")
        		.loginbtn();
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
      
    }
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Groups  Side Navigation is  Present")
    @Story("Test Verify Groups Main View")
	@Test (priority = 1)
	public void Navigate_to_Groups_List () {
		
		//Page instantiations
		GroupsPage GroupsPage = new GroupsPage(driver);
				
				
		//Page Methods
		System.out.println("Navigate to Groups");
		GroupsPage.Groupsbtn();
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Groups Main view is available")
    @Story("Test Verify Groups Main View")
	@Test(priority = 2)
	public void Verify_Groups_Main_View () {
		//Page instantations
		
		GroupsPage GroupsPage = new GroupsPage(driver);
		
		//Page Methods
		System.out.println("Verify Groups Main View");
		GroupsPage.VerifyGroupsMainView("Groups");
	}

	@Severity(SeverityLevel.BLOCKER)
    @Description("Input text to Search box")
    @Story("Search Groups")
	@Test (priority = 3)
	public void Perform_Search_Groups () {
		
		//Page instantations
		searchGroupsPage searchGroupsPage = new searchGroupsPage(driver);
		//Page methods
		System.out.println("Perform a Search: MATH ADDITION");
		searchGroupsPage.Search_a_Group("MATH ADDITION");
	}
	
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Search Results")
    @Story("Search Results")
	@Test (priority = 4)
	public void Verify_Search_Results () {
		//Page instantations
		searchGroupsPage searchGroupsPage = new searchGroupsPage(driver);
		//Page Methods
		System.out.println("Verify Search Results");
		searchGroupsPage.Search_Result("MATH ADDITION");
	}
}
