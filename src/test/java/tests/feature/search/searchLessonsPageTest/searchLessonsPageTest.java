package tests.feature.search.searchLessonsPageTest;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.IHookable;
import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import pages.DashboardPage;
import pages.feature.lessons.*;
import pages.feature.search.searchLessonsPage.searchLessonsPage;
import tests.BaseTest;

@Epic("Search Lessons")
@Feature("Search")

public class searchLessonsPageTest extends BaseTest implements IHookable {
	@Severity(SeverityLevel.BLOCKER)
    @Description("")
    @Story("")
	@Test (priority = 0)
	public void Teacher_Valid_Login_Valid_UserName_and_Password () {

        //*************PAGE INSTANTIATIONS*************
        DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        System.out.println("Login to Eduhex");
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("admin", "user")
        		.loginbtn();
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
      
    }
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Lessons  Side Navigation is  Present")
    @Story("Test Verify Lessons Main View")
	@Test (priority = 1)
	public void Navigate_to_Lessons_List () {
		
		//Page instantiations
		LessonsPage LessonsPage = new LessonsPage(driver);
				
				
		//Page Methods
		System.out.println("Navigate to Lessons");
		LessonsPage.Lessonsbtn();
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Lessons Main view is available")
    @Story("Test Verify Lessons Main View")
	@Test(priority = 2)
	public void Verify_Lessons_Main_View () {
		//Page instantations
		
		LessonsPage LessonsPage = new LessonsPage(driver);
		
		//Page Methods
		System.out.println("Verify Lessons Main View");
		LessonsPage.VerifyLessonsMainView("Lessons");
	}

	@Severity(SeverityLevel.BLOCKER)
    @Description("Input text to Search box")
    @Story("Search Lessons")
	@Test (priority = 3)
	public void Perform_Search_Class () {
		
		//Page instantations
		searchLessonsPage searchLessonsPage = new searchLessonsPage(driver);
		//Page methods
		System.out.println("Perform a Search: MATH ADDITION");
		searchLessonsPage.Search_a_Lesson("MATH ADDITION");
	}
	
	
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Search Results")
    @Story("Search Results")
	@Test (priority = 4)
	public void Verify_Search_Results () {
		//Page instantations
		searchLessonsPage searchLessonsPage = new searchLessonsPage(driver);
		//Page Methods
		System.out.println("Verify Search Results");
		searchLessonsPage.Search_Result("MATH ADDITION");
	}


}
