package tests.feature.exams;

import org.testng.annotations.Test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import pages.feature.exams.ExamsPage;
import tests.BaseTest;

@Epic("Exams Main View and Navigation")
@Feature("Exams")

public class ExamsPageTest extends BaseTest implements IHookable {
	
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 0)
	public void Valid_Login_Valid_Username_and_Password () {
		//*************PAGE INSTANTIATIONS*************
		DashboardPage DashboardPage = new DashboardPage(driver);
		
		
		//*************PAGE METHODS********************
		System.out.println("Login to EduHex");
		DashboardPage.goToDashboardPage()
		.goToLoginPage()
		.loginToEduhex("admin", "user")
		.loginbtn();
	}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Exams Side Navigation label is Present")
    @Story("Test Verify Exams Main View")
	@Test (priority = 1)
	public void Navigate_to_Exams () {
		//Page instantations
		ExamsPage ExamsPage = new ExamsPage(driver);
		
		//Page methods
		System.out.println("Click Exams Side Navigation");
		ExamsPage.Examsbtn();
	}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Exams Template Side Navigation label is Present")
    @Story("Test Verify Exams Main View")
	@Test (priority = 2)
	public void Navigate_to_Exams_Template () {
		//Page instantations
		ExamsPage ExamsPage = new ExamsPage(driver);
		
		//Page methods
		System.out.println("Click Exams Template");
		ExamsPage.Exams_Templatebtn();
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Exams Main view is available")
    @Story("Test Verify Exams Main View")
	@Test (priority = 3)
	public void Verify_Exams_Main_View () {
		//Page instantations
		ExamsPage ExamsPage = new ExamsPage (driver);
		
		//Page methods
		System.out.println("Verify Exams Template Main View");
		ExamsPage.Verify_Exams_Main_View("Exam Templates");
		
	}

}
