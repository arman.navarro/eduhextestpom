package tests.feature.groups;

import org.testng.annotations.Test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import pages.feature.exams.ExamsPage;
import pages.feature.groups.GroupsPage;
import tests.BaseTest;

@Epic("Groups Main View and Navigation")
@Feature("Data Groups")

public class GroupsPageTest extends BaseTest implements IHookable {
	
	
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 0)
	public void Valid_Login_Valid_Username_and_Password () {
		//*************PAGE INSTANTIATIONS*************
		DashboardPage DashboardPage = new DashboardPage(driver);
		
		
		//*************PAGE METHODS********************
		System.out.println("Login to EduHex");
		DashboardPage.goToDashboardPage()
		.goToLoginPage()
		.loginToEduhex("admin", "user")
		.loginbtn();
	}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Groups Side Navigation label is Present")
    @Story("Test Verify Groups Main View")
	@Test (priority = 1)
	public void Navigate_to_Groups () {
		//Page instantations
		GroupsPage GroupsPage = new GroupsPage(driver);
		
		//Page methods
		System.out.println("Click Groups Side Navigation");
		GroupsPage.Groupsbtn();
	}

}
