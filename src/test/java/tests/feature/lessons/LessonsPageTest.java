package tests.feature.lessons;


import org.testng.annotations.Test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import tests.BaseTest;
import pages.feature.lessons.LessonsPage;


@Epic("Lessons Main View and Navigation")
@Feature("Lessons")
public class LessonsPageTest extends BaseTest implements IHookable {
	
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 0)
	public void Valid_Login_Valid_Username_and_Password () {
		//*************PAGE INSTANTIATIONS*************
		DashboardPage DashboardPage = new DashboardPage(driver);
		
		
		//*************PAGE METHODS********************
		System.out.println("Login to EduHex");
		DashboardPage.goToDashboardPage()
		.goToLoginPage()
		.loginToEduhex("admin", "user")
		.loginbtn();
	}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Lessons label Side Navigation Present")
    @Story("Test Verify Lessons Main View")
	@Test (priority = 1)
	public void Navigate_to_Lessons_List () {
		//*************PAGE INSTANTIATIONS*************
	LessonsPage LessonsPage = new LessonsPage(driver);
					
		//*************PAGE METHODS********************
	LessonsPage.Lessonsbtn();
	
	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Lessons Main view is viewable")
    @Story("Test Verify Lessons Main View")
	@Test (priority = 2)
	public void Verify_Lessons_Main_View () {
		//PAge instantiations
		LessonsPage LessonsPage = new LessonsPage(driver);
		
		//Page Methods
		LessonsPage.VerifyLessonsMainView("Lessons");
	}

}
