package tests.feature.students;

import org.testng.annotations.Test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import tests.BaseTest;
import pages.feature.students.StudentsPage;

@Epic("Students Main View and Navigation")
@Feature("Students")
public class StudentsPageTest extends BaseTest implements IHookable {
	
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test (priority = 0)
	public void Valid_Login_Valid_Username_and_Password () {
	//*************PAGE INSTANTIATIONS*************
	DashboardPage DashboardPage = new DashboardPage(driver);
	
	
	//*************PAGE METHODS********************
	System.out.println("Login to EduHex");
	DashboardPage.goToDashboardPage()
	.goToLoginPage()
	.loginToEduhex("admin", "user")
	.loginbtn();
}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Users Side Navigation is Present")
    @Story("Navigate to Students Main View")
	@Test (priority = 1)
	public void Navigate_to_Users_Side_Navigation () {
		
		//Page Instantations
		StudentsPage StudentsPage = new StudentsPage(driver);
		
		//Page Methods
		System.out.println("Click Side Navigation: Users");
		StudentsPage.Users();

	}
	@Severity(SeverityLevel.BLOCKER)
    @Description("Verify Students Side Navigation is Present")
    @Story("Navigate to Students Main View")
	@Test (priority = 2)
	public void Navigate_to_Students () {
		
		//Page Instantations
		StudentsPage StudentsPage = new StudentsPage(driver);
		
		//Page Methods
		System.out.println("Navigate to Users: Students");
		StudentsPage.Studentsbtn();

	}
	@Severity(SeverityLevel.CRITICAL)
    @Description("Verify Students Main view is viewable")
    @Story("Test Verify Students Main View")
	@Test (priority = 3)	
	public void Verify_Students_Main_View () {
			//Page instantations
		StudentsPage StudentsPage = new StudentsPage(driver);
		
		
		//Page methods
		System.out.println("Verifying Students Main view");
		StudentsPage.VerifyStudentsMainView("Students");
		}
	}

