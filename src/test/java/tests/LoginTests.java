package tests;


import org.testng.annotations.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import tests.BaseTest;

@Epic("Valid and Invalid Login Scenarios")
@Feature("Login")
public class LoginTests extends BaseTest implements IHookable {
    
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Funtionality")
    @Story("Test Verify Valid Login")
	@Test (priority = 0)
    public void Valid_Login_Valid_UserName_and_Password () {

        //*************PAGE INSTANTIATIONS*************
        DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("admin", "user")
        		.loginbtn();
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"))
                //.verifyLoginPassword(("E-posta adresiniz veya şifreniz hatalı"));
      
    }
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Invalid Login: Empty Username and Password")
    @Story("Test Verify Invalid Login")
    @Test (priority = 1)
    public void Invalid_Login_Empty_Username_and_Empty_Password () {
        //*************PAGE INSTANTIATIONS*************
    	DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("","")
        		.loginbtn();
                //.verifyLoginUserName("Lütfen e-posta adresinizi girin.")
                //.verifyLoginPassword("Bu alanın doldurulması zorunludur.");
    }
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Invalid Login: Empty Username and Correct Password")
    @Story("Test Verify Invalid Login")
    @Test (priority = 2)
    public void Invalid_Login_Empty_Username_and_Correct_Password () {
    	 //*************PAGE INSTANTIATIONS*************
    	DashboardPage DashboardPage = new DashboardPage(driver);

        //*************PAGE METHODS********************
        DashboardPage.goToDashboardPage()
                .goToLoginPage()
                .loginToEduhex("","admin")
        		.loginbtn();
                //.verifyLoginUserName("Lütfen e-posta adresinizi girin.")
                //.verifyLoginPassword("Bu alanın doldurulması zorunludur.");
    }
    	
}