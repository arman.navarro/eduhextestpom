package tests;

import org.testng.annotations.Test;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.testng.IHookable;
import pages.DashboardPage;
import pages.LogoutMe;
import tests.BaseTest;


@Epic("Logout Scenarios")
@Feature("Login and Logout")
public class LogoutTest extends BaseTest implements IHookable {
	
    @Severity(SeverityLevel.BLOCKER)
    @Description("Verify Valid Login Functionality")
    @Story("Test Verify Valid Login")
	@Test(priority = 0)
	public void Valid_LoginTest_Valid_Username_Password () {
	//*************PAGE INSTANTIATIONS*************
	DashboardPage DashboardPage = new DashboardPage(driver);
	
	
	//*************PAGE METHODS********************
	DashboardPage.goToDashboardPage()
	.goToLoginPage()
	.loginToEduhex("admin", "user")
	.loginbtn();

	}
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify Valid Logout Functionality")
    @Story("Test Verify Valid Logout")
	@Test (priority = 1)
	public void Logout_Test () {
		//*************PAGE INSTANTIATIONS*************
		LogoutMe LogoutMe = new LogoutMe(driver);
		
		//*************PAGE METHODS********************
		LogoutMe.Logoutbtn();
		
	}
}
